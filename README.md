# README

## Indefinite hiatus
Due to the nature of a developing programmer my interests have shifted into other projects. Splash from now on is in an indefinite hiatus. One day I might return to continue to work on the project which is why I am not archiving the repo.

Splash in it's current state is made up of more than 3 years of my code, which has overtime changed and gotten better. Currently the project needs a full rewrite. Other thing is that I don't like relying on SFML, so a POTENTIAL future rewrite wound require me having sufficient understanding of OpenGL as a minumum.

## About the project 
Splash was created to solve a problem I myself had - not having a good enough alternative to Paint(or Paint 3D) after transitioning from Window to Linux. A software program that I could open in a matter of a second I could make a simple image. Splash tries to do exactly that - get out your way, be it if you want to just draw around for hours for fun or make a simple drawing in a hot second, Splash does that.

## Features

- Cross-platform - run Splash on Windows, Mac OS or Linux. //planned(only Linux currently)

- Tools - Spalsh has a simple tool set:
	- Pen - your most basic tool for drawing lines.
	- Eraser
	- Color Picker(pipette) //planned
	- Circle Shape
	- Rectangle Shape //planned
	- Shape Tool - define multiple points on the canvas to draw a shape //planned
	- Bucket
	
- Themes - Splash comes with two themes - Dark and Light and defaults to Light. If none of those tickle your fancy you may try making your own theme in the themes folder in the Splash directory. //planned

