#ifndef RESOURCES_H
#define RESOURCES_H

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <TGUI/TGUI.hpp>

#include "PenTool.h"
#include "CircleTool.h"

#include "Canvas.h"     
#include "Config.h"

class Resources
{
    public:
        Resources(sf::RenderWindow&);

        void createSidePanel();
        void createMenuBar();

        void loadToolTextures();

        void addToVertexCache(sf::VertexArray);
        void addToDynamicCache(sf::VertexArray);

        std::vector<sf::VertexArray> getVertexCache();
        std::vector<sf::VertexArray> getDynamicCache();

        Canvas& getCanvas();

        tgui::Gui& getGUI();

        void updateCurrentCanvas();

        void clearDynamicCache();
        void clearVertexCache();

    private:
        tgui::Gui gui;

        Config conf;
        Canvas canvas;

        std::vector<sf::VertexArray> vertexCache;
        std::vector<sf::VertexArray> dynamicCache;

        //Button icons
        tgui::Texture buttonIdle;
        tgui::Texture buttonHover;
        tgui::Texture buttonPressed;

        //Tool icon textures
        tgui::Texture penTexture;
        tgui::Texture eraserTexture;
        tgui::Texture bucketTexture;
        tgui::Texture circleTexture;
};

#endif // RESOURCES_H
