#ifndef COLORPICKER_H
#define COLORPICKER_H

#include <iostream>
#include <cmath>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <TGUI/TGUI.hpp>

#include <ContextSettings.h>

class ColorPicker
{
    public:
        ColorPicker();

    private:
    	///////////////////////////////
    	///		   functions		///
    	///////////////////////////////
    	void init();
        void loadTextures();
        void loadIcon();
    	void initWindow();

    	void loop();
    	void handleSelection(sf::Vector2f);
        void handleSelected();

        void updatePickerColor(float);
        sf::Color getRulerColor(sf::Vector2f);
        sf::Color getPickerColor(sf::Vector2f);

    	void handlePicker_Input(sf::Vector2f);
    	void handleRuler_Input(sf::Vector2f);

    	void createPicker(float);
    	void createRuler();

    	sf::Color convertHSVtoRGB(float, float, float);
    	float convertRGBtoHSV(sf::Color);

    	///////////////////////////////
    	///	 variables and objects  ///
    	///////////////////////////////
    	sf::RenderWindow window;
    	sf::Event event;

        ContextSettings settings;

        tgui::Gui gui;
        tgui::Texture buttonIdle;
        tgui::Texture buttonHover;
        tgui::Texture buttonPressed;

    	sf::VertexArray ruler{sf::Points, 15*510};
    	sf::VertexArray picker{sf::Points, 255*255};
    	sf::CircleShape hueSelector, colorSelector;

    	enum selectedType{None, Ruler, Picker};
    	selectedType selected;
};

#endif // COLORPICKER_H
