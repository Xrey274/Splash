#ifndef BRUSHES_H
#define BRUSHES_H

#include <iostream>
#include <string>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "Resources.h"
#include "Canvas.h"

#include "PenTool.h"
#include "CircleTool.h"

#include "ContextSettings.h"

class Brushes
{
    public:
        Brushes();
        enum brushTypes {Pen, Eraser, Bucket, Pipette, ShapeTool, Circle};

        void giveResource(Resources&);

        static void setType(brushTypes);

        static brushTypes getType();

        void onMousePressed(sf::Vector2f);
		void onMouseMoved(sf::Vector2f);
		void onMouseReleased(sf::Vector2f);

    private:
        static brushTypes brush;

        PenTool penTool;
        CircleTool circleTool;

        Resources* resource;
		ContextSettings settings;
};

#endif // BRUSHES_H
