#ifndef CIRCLETOOL_H
#define CIRCLETOOL_H

#include <iostream>
#include <cmath>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "ContextSettings.h"
#include "Config.h"

#include "Circle.h"

class CircleTool
{
   public:
        CircleTool();

        bool isSelectingSize();

        sf::VertexArray getCircle();

        void onMousePressed(sf::Vector2f);
        void onMouseMoved(sf::Vector2f);
        void onMouseReleased();
        
    private:
        //private functions
        void calculateRadius(sf::Vector2f);
        int calculateThickness(int);

        //class objects
        Circle circle;

        ContextSettings settings;
        Config conf;

        //private variables
        bool selectionMode = false;

        sf::Color selectionFillColor{176, 214, 242, 235};
        sf::Color selectionOutlineColor{124, 170, 204};

};

#endif // CIRCLETOOL_H
