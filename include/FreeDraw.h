#ifndef FREEDRAW_H
#define FREEDRAW_H

#include <iostream>
#include <cmath>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "Circle.h"
#include "ContextSettings.h"

class FreeDraw
{
    public:
        FreeDraw();

        sf::VertexArray& getLineDots();
        sf::VertexArray& getLineConnectors();

        size_t getLineDots_size();
        size_t getLineConnectors_size();
        bool isDrawing();

        void onMousePressed(sf::Vector2f);
        void onMouseMoved(sf::Vector2f);
        void onMouseReleased();

        void clear();

    private:
        //Function Declarations
        void addFirstDot(sf::Vector2f, float);
        void addDot(sf::Vector2f, float);
        void addConnectorLine();

        void savePositions(sf::Vector2f, float);

        ContextSettings settings;

        //Variables
        sf::Vector2f lastDot_position;
        sf::Vector2f previousDot_position;

        float lastDot_radius;
        float previousDot_radius;

        bool isFirst = true;

        sf::Color color;

    	sf::VertexArray dots;
        sf::VertexArray connectorLines;
};

#endif // FREEDRAW_H
