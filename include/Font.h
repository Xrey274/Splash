#ifndef FONT_H
#define FONT_H

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

class Font
{
    public:
        Font();

        sf::Font& get();

    private:
        static sf::Font font;

};

#endif // FONT_H
