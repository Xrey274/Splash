#ifndef PENTOOL_H
#define PENTOOL_H

#include <iostream>
#include <cmath>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "Circle.h"

#include "ContextSettings.h"
#include "Config.h"

class PenTool
{
    public:
        PenTool();

        bool isDrawing();

        sf::VertexArray onMousePressed(sf::Vector2f);
        sf::VertexArray onMousePressed(sf::Vector2f, sf::Color);
        sf::VertexArray onMouseMoved(sf::Vector2f);
        sf::VertexArray onMouseMoved(sf::Vector2f, sf::Color);
        void onMouseReleased();

    private:
        //Function Declarations
        Circle newCircle(sf::Vector2f, float);
        sf::VertexArray newDot(sf::Vector2f, float);
        void newConnector(sf::VertexArray&);

        void savePositions(Circle);

        //Context Settings object
        ContextSettings settings;
        Config conf;

        //Variables
        sf::VertexArray vertices;
        sf::Color fillColor;

        std::vector<Circle> oldDots;
        bool drawingState;
};

#endif // PENTOOL_H
