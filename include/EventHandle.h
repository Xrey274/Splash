#ifndef EVENTHANDLE_H
#define EVENTHANDLE_H

#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <TGUI/TGUI.hpp>

#include "Resources.h"
#include "Brushes.h"
#include "ColorPicker.h"

class EventHandle
{
    public:
        EventHandle(Resources&);

        void handleEvent(sf::RenderWindow&, sf::Event, sf::Vector2f);

        void handleLeftButton_Pressed(sf::Vector2f);
        void handleMouse_Moved(sf::Vector2f);
        void handleLeftButton_Released(sf::Vector2f);

        void setBrushType(Brushes::brushTypes);

        void handleMenuBar(std::string);
        void saveCanvasToFile();

    private:
    	Resources* resource;

        Brushes brush;

        sf::Color defaultColor{0, 0, 0, 255};
};

#endif // EVENTHANDLE_H
