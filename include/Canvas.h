#ifndef CANVAS_H
#define CANVAS_H

#include <string>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "PenTool.h"
#include "CircleTool.h"

#include "Config.h"


class Canvas : public sf::Sprite
{
    public:
        Canvas();
        Canvas(sf::Vector2f, sf::Vector2f, sf::Color, sf::ContextSettings);

        bool contains(sf::Vector2f);

        sf::Vector2u getSize();
        sf::Color getBackgroundColor();

        void setSize(sf::Vector2f);
        void setLocation(sf::Vector2f);

        void clear();
        void clear(sf::Color);
        void draw(std::vector<sf::VertexArray>);
        void display();

        sf::RenderTexture& getRenderTexture();
        sf::Image getAsImage();

        void update();
    private:
    	sf::RenderTexture texture;

    	sf::Color backgroundColor;

        Config conf;
};

#endif // CANVAS_H
