#ifndef CIRCLE_H
#define CIRCLE_H

#include <iostream>
#include <cmath>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

class Circle : public sf::CircleShape
{
   public:
        Circle();

        bool contains(sf::Vector2f);

        sf::VertexArray rasterize();
};  

#endif // CIRCLE_H
