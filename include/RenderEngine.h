#ifndef	RENDERENGINE_H
#define RENDERENGINE_H

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <TGUI/TGUI.hpp>

#include "Canvas.h"
#include "Resources.h"
#include "EventHandle.h"
#include "Config.h"

class RenderEngine
{
	public:
		RenderEngine();

		void loadIcon(sf::RenderWindow&);

		void loop();

		void clear();
		void clear(sf::Color);
		void draw(Resources&);
		void display(Resources&);

	private:
		sf::RenderWindow window;
		Config conf;
};	

#endif //RENDERENGINE_H