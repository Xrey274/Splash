#ifndef CONTEXTSETTINGS_H
#define CONTEXTSETTINGS_H

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

class ContextSettings
{
    public:
        ContextSettings();

        sf::Color getFillColor();
        sf::Color getOutlineColor();
       	int getOutlineThickness();

       	void setFillColor(sf::Color color);
       	void setOutlineColor(sf::Color color);
       	void setOutlineThickness(int thickness);

    private:
    	static sf::Color fillColor;
    	static sf::Color outlineColor;

    	static int outlineThickness;
};

#endif // CONTEXTSETTINGS_H
