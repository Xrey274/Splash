#ifndef CONFIG_H
#define CONFIG_H

#include <SFML/System.hpp>

class Config
{
    public:
        Config();

        void setWindowSize(sf::Vector2f size);
       	void setOffset(sf::Vector2f offset);

        void setFramerateCap(unsigned int);
        void setIsVsync(bool);

        ////////////////////////////////////

        sf::Vector2f getWindowSize();
        sf::Vector2f getOffset();

        std::string getHeaderBar_String();

        unsigned int getFramerateCap();
        bool getIsVsync();

    private:
    	static sf::Vector2f windowSize;
      static sf::Vector2f canvasOffset;

      static std::string name, version, devStage;

      static unsigned int framerateCap;
      static bool isVsync;
};

#endif // CONFIG_H
