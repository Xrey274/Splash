#include "include/Circle.h"

Circle::Circle()
{

}

//check if the coordinates of an object are inside the circle
//optimised version of getGlobalboundaries().contains() for circles
bool Circle::contains(sf::Vector2f objectPosition)
{
    int xDelta = objectPosition.x - getPosition().x;
    int yDelta = objectPosition.y - getPosition().y;

    int xySquared = (xDelta * xDelta) + (yDelta * yDelta);

    return (xySquared > pow(getRadius() + getOutlineThickness(), 2)) ? false : true; 
}

sf::VertexArray Circle::rasterize()
{
    sf::VertexArray circleArray;
    circleArray.setPrimitiveType(sf::TriangleFan);

    for(int i = 0; i < getPointCount(); ++i)
    {
        sf::Vertex point = getTransform().transformPoint(getPoint(i));
        point.color = getFillColor();

        circleArray.append(point);
    }

    return circleArray;
}