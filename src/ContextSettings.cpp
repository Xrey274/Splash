#include "include/ContextSettings.h"

sf::Color ContextSettings::fillColor = sf::Color::Black;
sf::Color ContextSettings::outlineColor = sf::Color::Black;

int ContextSettings::outlineThickness = 0;

ContextSettings::ContextSettings()
{

}

sf::Color ContextSettings::getFillColor() 
{
	return fillColor;
}

sf::Color ContextSettings::getOutlineColor() 
{
	return outlineColor;
}

int ContextSettings::getOutlineThickness() 
{
	return outlineThickness;
}

//////////////////////////////////////////////
//                                          //
//                                          //
//                                          //
//                                          //
//////////////////////////////////////////////

void ContextSettings::setFillColor(sf::Color color) 
{
	fillColor = color;
}

void ContextSettings::setOutlineColor(sf::Color color) 
{
	outlineColor = color;
}

void ContextSettings::setOutlineThickness(int thickness) 
{
	outlineThickness = thickness;
}