#include "include/RenderEngine.h"


RenderEngine::RenderEngine()
{
	loop();
}

void RenderEngine::loadIcon(sf::RenderWindow& window)
{
    tgui::Texture icon_texture;
    icon_texture.load("icons/svg/logo-circle.svg");

    sf::Texture icon;
    icon_texture.getData()->svgImage->rasterize(icon, sf::Vector2u(512, 512));
    window.setIcon(icon.getSize().x, icon.getSize().y, icon.copyToImage().getPixelsPtr());
}

void RenderEngine::loop()
{
	sf::VideoMode mode = sf::VideoMode::getDesktopMode();
	conf.setWindowSize(sf::Vector2f(mode.width, mode.height));

    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;

	window.create(
            sf::VideoMode(mode.width, mode.height),               //get the resolution of current monitor and use it as size for window

            conf.getHeaderBar_String(),                           //title bar name

            sf::Style::Default,                                   //apply window style(default)

            settings
    );

    loadIcon(window);

    window.setVerticalSyncEnabled(conf.getIsVsync());
    if(!conf.getIsVsync())
    {
    	window.setFramerateLimit(conf.getFramerateCap());
    }

    //class objects
    Resources resource(window);
    EventHandle handle(resource);

    sf::Event event;

    while(window.isOpen())
    {
        sf::Vector2f mouseCoords = window.mapPixelToCoords(
                sf::Mouse::getPosition(window), window.getView()  //get mouse position relative to window in coordinates
        );

        while(window.pollEvent(event))
        {
            handle.handleEvent(window, event, mouseCoords);
        }

        clear(sf::Color(55, 55, 55));

        draw(resource);

        display(resource);
    }
}

void RenderEngine::clear()
{
	window.clear();
}

//overloaded function of clear that accepts a color
//alpha MUST always be 255
void RenderEngine::clear(sf::Color color)
{
	window.clear(color);
}

void RenderEngine::draw(Resources& resource)
{
	//draw to the canvas
	resource.getCanvas().draw(resource.getVertexCache());

	//update canvas texture
	resource.updateCurrentCanvas();

	//draw canvas sf::Sprite to window
	window.draw(resource.getCanvas());

	resource.getGUI().draw();

	//Circles are drawn directly to the window, as they need to dynamically change
    for(auto shape : resource.getDynamicCache())
    {
        window.draw(shape);
    }
    

	resource.clearVertexCache();
}

void RenderEngine::display(Resources& resource)
{
	resource.getCanvas().display();

	window.display();
}

