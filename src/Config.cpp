#include "include/Config.h"

sf::Vector2f Config::canvasOffset(0, 0);
sf::Vector2f Config::windowSize(0, 0);
 
std::string Config::name = "Splash";
std::string Config::version = "0.3.1.R7";
std::string Config::devStage = "Beta";

unsigned int Config::framerateCap = 500;
bool Config::isVsync = true;

Config::Config()
{

}

void Config::setWindowSize(sf::Vector2f size)
{
	windowSize = size;
}

void Config::setOffset(sf::Vector2f offset) 
{
	canvasOffset = offset;
}

void Config::setFramerateCap(unsigned int fps)
{
	framerateCap = fps;
}

void Config::setIsVsync(bool option)
{
	isVsync = option;
}

//////////////////////////////////////////////
//                                          //
//                                          //
//                                          //
//                                          //
//////////////////////////////////////////////

sf::Vector2f Config::getWindowSize()
{
	return windowSize;
}

sf::Vector2f Config::getOffset()
{
	return canvasOffset;
}

std::string Config::getHeaderBar_String()
{
	return name + " " + version + " " + devStage;
} 

unsigned int Config::getFramerateCap()
{
	return framerateCap;
}

bool Config::getIsVsync()
{
	return isVsync;
}