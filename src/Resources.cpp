#include "include/Resources.h"

Resources::Resources(sf::RenderWindow& window)
{
    gui.setTarget(window);
    gui.setFont("OpenSans-SemiBold.ttf");

    createSidePanel();
    createMenuBar();

    canvas.setSize(sf::Vector2f(1408, 792));
    canvas.setLocation(static_cast<sf::Vector2f>(window.getSize()));
    canvas.clear(sf::Color::White);

    conf.setOffset(sf::Vector2f(
            (window.getSize().x - canvas.getSize().x) / 2,  //offset for X axis

            (window.getSize().y - canvas.getSize().y) / 2   //offset for Y axis
        )
    );
}

void Resources::createSidePanel()
{
    loadToolTextures();

    auto panel = tgui::Panel::create();
    panel->getRenderer()->setBackgroundColor(sf::Color(41, 41, 41));
    panel->setSize(140, "100%");
    panel->setPosition(0, 0);

    auto label = tgui::Label::create();
    label->getRenderer()->setBackgroundColor(sf::Color(41, 41, 41));
    label->getRenderer()->setTextColor(sf::Color::White);
    label->setText("Tools");
    label->setTextSize(25);
    label->setPosition((panel->getSize().x - label->getSize().x) / 2, 5);
    label->setHorizontalAlignment(tgui::Label::HorizontalAlignment::Center);

    panel->add(label, "tools_label");

    //Pen Button
    auto pen_button = tgui::BitmapButton::create();
    pen_button->getRenderer()->setTexture(buttonIdle);
    pen_button->getRenderer()->setTextureHover(buttonHover);
    pen_button->getRenderer()->setTextureDown(buttonPressed);
    pen_button->getRenderer()->setBorders(0);
    pen_button->setImage(penTexture);
    pen_button->setImageScaling(0.85);
    pen_button->setSize(45, 45);
    pen_button->setPosition(15, 60);

    panel->add(pen_button, "pen_button");

    //Eraser Button
    auto eraser_button = tgui::BitmapButton::create();
    eraser_button->getRenderer()->setTexture(buttonIdle);
    eraser_button->getRenderer()->setTextureHover(buttonHover);
    eraser_button->getRenderer()->setTextureDown(buttonPressed);
    eraser_button->getRenderer()->setBorders(0);
    eraser_button->setImage(eraserTexture);
    eraser_button->setImageScaling(0.85);
    eraser_button->setSize(45, 45);
    eraser_button->setPosition(80, 60);
    
    panel->add(eraser_button, "eraser_button");

    //Bucket Button
    auto bucket_button = tgui::BitmapButton::create();
    bucket_button->getRenderer()->setTexture(buttonIdle);
    bucket_button->getRenderer()->setTextureHover(buttonHover);
    bucket_button->getRenderer()->setTextureDown(buttonPressed);
    bucket_button->getRenderer()->setBorders(0);
    bucket_button->setImage(bucketTexture);
    bucket_button->setImageScaling(0.85);
    bucket_button->setSize(45, 45);
    bucket_button->setPosition(15, 120);

    panel->add(bucket_button, "bucket_button");

    //Pipette Button
    auto circle_button = tgui::BitmapButton::create();
    circle_button->getRenderer()->setTexture(buttonIdle);
    circle_button->getRenderer()->setTextureHover(buttonHover);
    circle_button->getRenderer()->setTextureDown(buttonPressed);
    circle_button->getRenderer()->setBorders(0);
    circle_button->setImage(circleTexture);
    circle_button->setImageScaling(0.9);
    circle_button->setSize(45, 45);
    circle_button->setPosition(80, 120);

    panel->add(circle_button, "circle_button");

    gui.add(panel, "sidepanel");
}

void Resources::createMenuBar()
{
    auto menuBar = tgui::MenuBar::create();
    menuBar->getRenderer()->setBackgroundColor(sf::Color(41, 41, 41));
    menuBar->getRenderer()->setSelectedBackgroundColor(sf::Color(71, 71, 71));
    menuBar->getRenderer()->setTextColor(sf::Color::White);
    menuBar->setSize("100%", 35);
    menuBar->setPosition(140, 0);
    menuBar->setMinimumSubMenuWidth(300 / 4);

    menuBar->addMenu("File");;
    menuBar->addMenuItem("Open");
    menuBar->addMenuItem("Save");
    menuBar->addMenuItem("Discard");

    menuBar->addMenu("Color Picker");
    menuBar->addMenuItem("Open");      

    menuBar->addMenu("Pallete");

    menuBar->addMenu("Clear Canvas");

    menuBar->addMenu("Settings");

    menuBar->addMenu("About");

    gui.add(menuBar, "menubar");
}

void Resources::loadToolTextures()
{
    //LOAD BUTTON TEXTURES
    buttonIdle.load("icons/svg/button_idle.svg");
    buttonHover.load("icons/svg/button_hover.svg");
    buttonPressed.load("icons/svg/button_pressed.svg");

    //LOAD ICON TEXTURES
    penTexture.load("icons/svg/pen_tool.svg");

    eraserTexture.load("icons/svg/eraser_tool.svg");

    bucketTexture.load("icons/svg/bucket_tool.svg");

    circleTexture.load("icons/svg/circle_tool.svg");
}

void Resources::addToVertexCache(sf::VertexArray entry)
{
    vertexCache.push_back(entry);
}

void Resources::addToDynamicCache(sf::VertexArray entry)
{
    dynamicCache.push_back(entry);
}

std::vector<sf::VertexArray> Resources::getVertexCache()
{
    return vertexCache;
}

std::vector<sf::VertexArray> Resources::getDynamicCache()
{
    return dynamicCache;
}

Canvas& Resources::getCanvas()
{
    return canvas;
}


tgui::Gui& Resources::getGUI()
{
    return gui;
}

void Resources::updateCurrentCanvas()
{
    canvas.update();
}

void Resources::clearDynamicCache()
{
    dynamicCache.clear();
}

void Resources::clearVertexCache()
{
    vertexCache.clear();
}