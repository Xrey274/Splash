#include "include/Font.h"

#include <iostream>

sf::Font Font::font;

Font::Font()
{
    if(!font.loadFromFile("OpenSans-SemiBold.ttf"))
    {
        std::cout<<"Error: Could not load font \n" 
        	"Check if font exists in folder."<<std::endl;
    }
}

sf::Font& Font::get()
{
    return font;
}
