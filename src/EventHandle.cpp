#include "include/EventHandle.h"

EventHandle::EventHandle(Resources& object)
{
	resource = &object;

    brush.giveResource(*resource);

    tgui::Panel::Ptr panel = resource->getGUI().get<tgui::Panel>("sidepanel");
    tgui::MenuBar::Ptr menuBar = resource->getGUI().get<tgui::MenuBar>("menubar");

    /////////////////////////////////////
    ///            connect            ///
    ///      buttons to functions     ///
    /////////////////////////////////////

    tgui::BitmapButton::Ptr pen_button = panel->get<tgui::BitmapButton>("pen_button");
    pen_button->connect("pressed", &EventHandle::setBrushType, this, Brushes::Pen);

    tgui::BitmapButton::Ptr eraser_button = panel->get<tgui::BitmapButton>("eraser_button");
    eraser_button->connect("pressed", &EventHandle::setBrushType, this, Brushes::Eraser);

    tgui::BitmapButton::Ptr circle_button = panel->get<tgui::BitmapButton>("circle_button");
    circle_button->connect("pressed", &EventHandle::setBrushType, this, Brushes::Circle);

    /////////////////////////////////////
    ///            connect            ///
    ///     ColorPicker to menuBar    ///
    /////////////////////////////////////

    menuBar->connectMenuItem(sf::String("Color Picker"), sf::String("Open"), &EventHandle::handleMenuBar, this, "Color Picker");

    /////////////////////////////////////
    ///            connect            ///
    ///         File to menuBar       ///
    /////////////////////////////////////

    menuBar->connectMenuItem(sf::String("File"), sf::String("Save"), &EventHandle::saveCanvasToFile, this);
}

//TODO: Add other brush types along side their full functionality

void EventHandle::handleEvent(sf::RenderWindow& window, sf::Event event, sf::Vector2f mouseCoords)
{
    switch(event.type)
    {
        case sf::Event::Closed:
            window.close();

            break;
        case sf::Event::Resized:
        {
            sf::FloatRect viewRect(
                0,
                0,

                event.size.width,
                event.size.height
            );

            sf::View newView(viewRect);

            window.setView(newView);
            resource->getGUI().setView(newView);

            break;   
        }
                    
        case sf::Event::MouseButtonPressed:
            if(event.mouseButton.button == sf::Mouse::Left)
            {
                handleLeftButton_Pressed(mouseCoords);
            }

            break;
        case sf::Event::MouseMoved:
            handleMouse_Moved(mouseCoords);
                    
            break;
        case sf::Event::MouseButtonReleased:
            if(event.mouseButton.button == sf::Mouse::Left)
            {
                handleLeftButton_Released(mouseCoords);
            }
                    
            break;
    }

    resource->getGUI().handleEvent(event);
}

void EventHandle::handleLeftButton_Pressed(sf::Vector2f mouseCoords)
{
    brush.onMousePressed(mouseCoords);
}

void EventHandle::handleMouse_Moved(sf::Vector2f mouseCoords)
{
    brush.onMouseMoved(mouseCoords);
}

void EventHandle::handleLeftButton_Released(sf::Vector2f mouseCoords)
{
    brush.onMouseReleased(mouseCoords);
}

void EventHandle::setBrushType(Brushes::brushTypes type)
{
    brush.setType(type);
}

void EventHandle::handleMenuBar(std::string type)
{
    if(type == "Color Picker")
    {
        ColorPicker wheel;
    }
}

void EventHandle::saveCanvasToFile()
{
    resource->getCanvas().getTexture()->copyToImage().saveToFile("Image.png");
}

