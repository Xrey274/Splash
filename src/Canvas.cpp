#include <iostream>

#include "include/Canvas.h"

//TODO: Maybe add mutliple canvases as tabs

Canvas::Canvas()
{

}

Canvas::Canvas(
              sf::Vector2f size,
              sf::Vector2f windowSize,
              sf::Color color,
              sf::ContextSettings settings
              ) : backgroundColor(color)
{
    texture.create(size.x, size.y, settings);

    setPosition(sf::Vector2f(
        (windowSize.x - size.x) / 2,
        (windowSize.y - size.y) / 2
        )
    );

    texture.clear(backgroundColor);
}

bool Canvas::contains(sf::Vector2f coords)
{
    return getGlobalBounds().contains(coords);
}

sf::Vector2u Canvas::getSize()
{
    return texture.getSize();
}

void Canvas::setSize(sf::Vector2f size)
{
    sf::ContextSettings textureSettings;
    //Currently broken for unknown reason. Uncomment at your own risk.
    textureSettings.antialiasingLevel = 0/*texture.getMaximumAntialiasingLevel()*/;

    texture.create(size.x, size.y, textureSettings);
}

void Canvas::setLocation(sf::Vector2f pos)
{
    conf.setOffset(
        sf::Vector2f((pos.x - getSize().x) / 2, (pos.y - getSize().y) / 2)
    );

    setPosition(sf::Vector2f(
        (pos.x - getSize().x) / 2,
        (pos.y - getSize().y) / 2
        )
    );
}

sf::Color Canvas::getBackgroundColor()
{
    return backgroundColor;
}

void Canvas::clear()
{
    texture.clear(backgroundColor);
}

void Canvas::clear(sf::Color color)
{
    backgroundColor = color;

    texture.clear(color);
}

void Canvas::draw(std::vector<sf::VertexArray> objects)
{
    for(auto i : objects)
    {
        texture.draw(i);
    }
}

sf::RenderTexture& Canvas::getRenderTexture()
{
    return texture;
}

sf::Image Canvas::getAsImage()
{
    return texture.getTexture().copyToImage();
}

void Canvas::display()
{
    texture.display();
}

void Canvas::update()
{
    setTexture(texture.getTexture());
}
