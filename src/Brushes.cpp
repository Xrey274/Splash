#include "include/Brushes.h"

Brushes::brushTypes Brushes::brush;

Brushes::Brushes()
{
    brush = Pen;
}

void Brushes::giveResource(Resources& _res)
{
    resource = &_res;
}

void Brushes::setType(Brushes::brushTypes type)
{
    brush = type;
}

Brushes::brushTypes Brushes::getType()
{
    return brush;
}

void Brushes::onMousePressed(sf::Vector2f mouseCoords)
{
    if(resource->getCanvas().contains(mouseCoords))
    {
        switch(brush)
        {
            case Pen:
                resource->addToVertexCache(penTool.onMousePressed(mouseCoords));

                break;
            case Eraser:
                resource->addToVertexCache(penTool.onMousePressed(mouseCoords, resource->getCanvas().getBackgroundColor()));

                break;
            case Bucket:

                break;
            case Pipette:

                break;
            case ShapeTool:

                break;
            case Circle:
                circleTool.onMousePressed(mouseCoords);

                resource->addToDynamicCache(circleTool.getCircle());

                break;
        }
    }
}

void Brushes::onMouseMoved(sf::Vector2f mouseCoords)
{
    switch(brush)
    {
        case Pen:
            if(penTool.isDrawing())
            {
                resource->addToVertexCache(penTool.onMouseMoved(mouseCoords));
            }

            break;
        case Eraser:
            if(penTool.isDrawing())
            {
                resource->addToVertexCache(penTool.onMouseMoved(mouseCoords, resource->getCanvas().getBackgroundColor()));
            }

            break;
        case Bucket:

            break;
        case Pipette:

            break;
        case ShapeTool:

            break;
        case Circle:
            if(circleTool.isSelectingSize())
            {
                circleTool.onMouseMoved(mouseCoords);

                resource->clearDynamicCache();
                resource->addToDynamicCache(circleTool.getCircle());
            }
            
            break;
    }
}

void Brushes::onMouseReleased(sf::Vector2f mouseCoords)
{
	switch(brush)
	{
		case Pen:
            penTool.onMouseReleased();
		
		    break;
		case Eraser:
            penTool.onMouseReleased();
		
		    break;
		case Bucket:
		
		    break; 
		case Pipette:
		
		    break;
		case ShapeTool:
		
		    break;
		case Circle:
            if(resource->getCanvas().contains(mouseCoords))
            {
                circleTool.onMouseReleased();

                resource->addToVertexCache(circleTool.getCircle());
                resource->clearDynamicCache();
            }

		    break;
	}	
}

//TODO: Fully implement brush switching
