#include "include/FreeDraw.h"

FreeDraw::FreeDraw()
{

}

float pythagoreanTheorem(float a, float b)
{
    return sqrt((a * a) + (b * b));
}

void FreeDraw::addFirstDot(sf::Vector2f mousePos, float radius)
{
    Circle dot;

    dot.setRadius(radius);
    dot.setOrigin(radius, radius);
    dot.setPointCount(radius * 10);
    dot.setPosition(mousePos);
    dot.setFillColor(settings.getFillColor());

    for(int i = 0; i < dot.getPointCount(); ++i)
    {
        sf::Vertex point = dot.getTransform().transformPoint(dot.getPoint(i));
        point.color = dot.getFillColor();

        dots.append(point);
    }

    lastDot_position = dot.getPosition();
    lastDot_radius   = dot.getRadius();
}

void FreeDraw::addDot(sf::Vector2f mousePos, float radius)
{
    Circle dot;

    dot.setRadius(radius);
    dot.setOrigin(radius, radius);
    dot.setPointCount(radius * 10);
    dot.setPosition(mousePos);
    dot.setFillColor(settings.getFillColor());

    for(int i = 0; i < dot.getPointCount(); ++i)
    {
        sf::Vertex point = dot.getTransform().transformPoint(dot.getPoint(i));
        point.color = dot.getFillColor();

        dots.append(point);
    }

    savePositions(dot.getPosition(), dot.getRadius());
}

void FreeDraw::addConnectorLine()
{
    float newX = lastDot_position.x;
    float newY = lastDot_position.y;

    float oldX = previousDot_position.x;
    float oldY = previousDot_position.y;

    sf::RectangleShape line;

    line.setSize(
            sf::Vector2f(pythagoreanTheorem(newX - oldX, newY - oldY),
                
            previousDot_radius * 2)
            );
    line.setOrigin(sf::Vector2f(0, line.getSize().y / 2));
    line.setPosition(oldX, oldY);

    //calculate arc-tangent
    double rotation_radians = atan2(newY - oldY, newX - oldX);

    //convert radians to degrees and set them as the rotation
    line.setRotation(rotation_radians * 180 / 3.14);

    line.setFillColor(settings.getFillColor());

    for(int i = 0; i < line.getPointCount(); ++i)
    {
        sf::Vertex point = line.getTransform().transformPoint(line.getPoint(i));

        point.color = settings.getFillColor();
        connectorLines.append(point);
    }
}

sf::VertexArray& FreeDraw::getLineDots()
{
    return dots;
}

sf::VertexArray& FreeDraw::getLineConnectors()
{
    return connectorLines;
}

size_t FreeDraw::getLineDots_size()
{
    return dots.getVertexCount();
}

size_t FreeDraw::getLineConnectors_size()
{
    return connectorLines.getVertexCount();
}

bool FreeDraw::isDrawing()
{
    return isFirst;
}

void FreeDraw::onMousePressed(sf::Vector2f mouseCoords)
{
    addFirstDot(mouseCoords - settings.getOffset(), 25);

    isFirst = false;
}

void FreeDraw::onMouseMoved(sf::Vector2f mouseCoords)
{
    addDot(mouseCoords - settings.getOffset(), 25);
}

void FreeDraw::onMouseReleased()
{
    clear();
    isFirst = true;
}

void FreeDraw::clear()
{
    dots.clear();
    connectorLines.clear();

    previousDot_position = sf::Vector2f(0, 0);
    previousDot_radius = 0;
}

void FreeDraw::savePositions(sf::Vector2f lastPosition, float lastRadius)
{
    previousDot_position = lastDot_position;
    previousDot_radius = lastDot_radius;

    lastDot_position = lastPosition;
    lastDot_radius = lastRadius;

    if(isFirst == false)
    {
        addConnectorLine();
    }
}