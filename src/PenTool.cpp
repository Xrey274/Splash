#include "include/PenTool.h"

PenTool::PenTool()
{
    drawingState = false;
}

float pythagoreanTheorem(float a, float b)
{
    return sqrt((a * a) + (b * b));
}

Circle PenTool::newCircle(sf::Vector2f position, float radius)
{
    Circle dot;

    dot.setRadius(radius);
    dot.setOrigin(radius, radius);
    dot.setPointCount(radius * 10);
    dot.setPosition(position);
    dot.setFillColor(fillColor);

    return dot;
}

sf::VertexArray PenTool::newDot(sf::Vector2f mousePos, float radius)
{
    sf::VertexArray temp = newCircle(mousePos, radius).rasterize();
    savePositions(newCircle(mousePos, radius));

    if(oldDots.size() == 2)
    {
        newConnector(temp);    
    }

    return temp;
}

void PenTool::newConnector(sf::VertexArray& temp)
{
    float newX = oldDots[0].getPosition().x;
    float newY = oldDots[0].getPosition().y;

    float oldX = oldDots[1].getPosition().x;
    float oldY = oldDots[1].getPosition().y;

    sf::RectangleShape line;
    line.setSize(
            sf::Vector2f(pythagoreanTheorem(newX - oldX, newY - oldY),
                
            oldDots[1].getRadius() * 2)
            );
    line.setOrigin(sf::Vector2f(0, line.getSize().y / 2));
    line.setPosition(oldX, oldY);

    //calculate arc-tangent
    double rotation_in_radians = atan2(newY - oldY, newX - oldX);

    //convert radians to degrees and set them as the rotation
    line.setRotation(rotation_in_radians * 180 / 3.14);

    for(int i = 0; i < line.getPointCount(); ++i)
    {
        sf::Vertex point = line.getTransform().transformPoint(line.getPoint(i));

        point.color = fillColor;
        temp.append(point);
    }
}

bool PenTool::isDrawing()
{
    return drawingState;
}

sf::VertexArray PenTool::onMousePressed(sf::Vector2f mouseCoords)
{
    drawingState = true;
    fillColor = settings.getFillColor();

    return newDot(mouseCoords - conf.getOffset(), 25);
}

sf::VertexArray PenTool::onMousePressed(sf::Vector2f mouseCoords, sf::Color color)
{
    drawingState = true;
    fillColor = color;

    return newDot(mouseCoords - conf.getOffset(), 25);
}

sf::VertexArray PenTool::onMouseMoved(sf::Vector2f mouseCoords)
{
    fillColor = settings.getFillColor();

    return newDot(mouseCoords - conf.getOffset(), 25);
}

sf::VertexArray PenTool::onMouseMoved(sf::Vector2f mouseCoords, sf::Color color)
{
    fillColor = color;

    return newDot(mouseCoords - conf.getOffset(), 25);
}

void PenTool::onMouseReleased()
{
    drawingState = false;

    vertices.clear();
    oldDots.clear();
}

void PenTool::savePositions(Circle circle)
{
    oldDots.push_back(circle);

    if(oldDots.size() > 2)
    {
        oldDots.erase(oldDots.begin());
    }
}