#include "include/CircleTool.h"

CircleTool::CircleTool()
{

}

//calculate the radius of the CircleTool based on how many pixels the mouse has moved since the CircleTool was created
void CircleTool::calculateRadius(sf::Vector2f mouseCoords)
{
    int radius = 0;

    radius = (circle.getPosition().x > mouseCoords.x) ?
        (circle.getPosition().x - mouseCoords.x) :
        
        (mouseCoords.x - circle.getPosition().x); //Calculate distance from origin | X Coord

    radius += (circle.getPosition().y > mouseCoords.y) ? 
        (circle.getPosition().y - mouseCoords.y) :

        (mouseCoords.y - circle.getPosition().y); //Calculate distance from origin | Y Coord

    circle.setRadius(radius);
}

int CircleTool::calculateThickness(int radius)
{
    int temp = ceil(radius < 0 ? radius / 10 : -radius / 10);

    return temp;
}

bool CircleTool::isSelectingSize()
{
    return selectionMode;
}

sf::VertexArray CircleTool::getCircle()
{
    return circle.rasterize();
}

void CircleTool::onMousePressed(sf::Vector2f mouseCoords)
{
    circle.setRadius(1);
    circle.setOrigin(circle.getRadius(), circle.getRadius());
    circle.setPosition(mouseCoords);

    circle.setFillColor(selectionFillColor);
    circle.setOutlineColor(selectionOutlineColor);

    circle.setPointCount(circle.getRadius() * 2);
    circle.setOutlineThickness(calculateThickness(circle.getRadius()));

    selectionMode = true;
}

void CircleTool::onMouseMoved(sf::Vector2f mouseCoords)
{
    calculateRadius(mouseCoords);
    circle.setOrigin(circle.getRadius(), circle.getRadius());

    circle.setPointCount(circle.getRadius() * 2);
    circle.setOutlineThickness(calculateThickness(circle.getRadius()));
}

void CircleTool::onMouseReleased()
{
    circle.setFillColor(settings.getFillColor());
    circle.setOutlineColor(settings.getOutlineColor());
    circle.setOutlineThickness(settings.getOutlineThickness());

    //compensate for canvas offset
    circle.move(-conf.getOffset());

    selectionMode = false;
}