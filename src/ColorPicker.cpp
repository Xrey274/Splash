#include "include/ColorPicker.h"

#include <iostream>

ColorPicker::ColorPicker()
{
	createRuler();
    createPicker(360);

    init();
    initWindow();

    loop(); 
}

void ColorPicker::init()
{
    gui.setTarget(window);
    gui.setFont("OpenSans-SemiBold.ttf");

	hueSelector.setRadius(8);
    hueSelector.setOrigin(hueSelector.getRadius(), hueSelector.getRadius());
    hueSelector.setPointCount(50);
    hueSelector.setFillColor(sf::Color::Red);
    hueSelector.setOutlineColor(sf::Color(70, 70, 70));
    hueSelector.setOutlineThickness(2);
    hueSelector.setPointCount(hueSelector.getRadius() * 8);
    hueSelector.setPosition(picker[255].position);

    colorSelector = hueSelector;
    colorSelector.setPosition(ruler[8].position);

    loadTextures();

    auto select_button = tgui::Button::create();

    select_button->getRenderer()->setTexture(buttonIdle);
    select_button->getRenderer()->setTextureHover(buttonHover);
    select_button->getRenderer()->setTextureDown(buttonPressed);
    select_button->getRenderer()->setBorders(0);
    select_button->getRenderer()->setTextColor(sf::Color::White);
    select_button->getRenderer()->setTextColorHover(sf::Color::White);
    select_button->getRenderer()->setTextColorDown(sf::Color::White);
    select_button->setText("Select");
    select_button->setTextSize(18);
    select_button->setSize(130, 60);
    select_button->setPosition(800, 850);

    select_button->connect("pressed", &ColorPicker::handleSelected, this);
    gui.add(select_button, "select_button");

    selected = None;
}

void ColorPicker::loadTextures()
{
    buttonIdle.load("icons/svg/button_idle.svg");
    buttonHover.load("icons/svg/button_hover.svg");
    buttonPressed.load("icons/svg/button_pressed.svg");
}

void ColorPicker::loadIcon()
{
    tgui::Texture icon_texture;
    icon_texture.load("icons/svg/logo-circle.svg");

    sf::Texture icon;
    icon_texture.getData()->svgImage->rasterize(icon, sf::Vector2u(512, 512));
    window.setIcon(icon.getSize().x, icon.getSize().y, icon.copyToImage().getPixelsPtr());
}

void ColorPicker::initWindow()
{
    sf::ContextSettings context;
    context.antialiasingLevel = 8;

	window.create(sf::VideoMode(750, 750), "Color Picker", sf::Style::Close, context);

    window.setVerticalSyncEnabled(true);

    loadIcon();
}

void ColorPicker::loop()
{
	while(window.isOpen())
	{
		sf::Vector2f mouseCoords = window.mapPixelToCoords(
				sf::Mouse::getPosition(window), window.getView()
			);

		handleSelection(mouseCoords);
	
        window.clear(sf::Color(45, 45, 45));

        window.draw(ruler);
        window.draw(picker);

        window.draw(hueSelector);
        window.draw(colorSelector);

        gui.draw();

        window.display();
    }
}

float ColorPicker::convertRGBtoHSV(sf::Color color)
{
    float hue = 360;

    float r = static_cast<float>(color.r);
    float g = static_cast<float>(color.g);
    float b = static_cast<float>(color.b);

    float Cmax = std::max(r, std::max(g, b));
    float Cmin = std::min(r, std::min(g, b));

    float delta = Cmax - Cmin;

    if(delta == 0)
    {
        hue = 0;
    }
    if(Cmax == r)
    {
        hue = 60.0 * ((g - b) / delta);
    }
    else if(Cmax == g)
    {
        hue = 60.0 * (((b - r) / delta) + 2);
    }
    else if(Cmax == b)
    {
        hue = 60.0 * (((r - g) / delta) + 4);
    }

    if(hue < 0)
    {
        hue += 360;
    }

    return hue;
}

sf::Color ColorPicker::convertHSVtoRGB(float h, float s, float v)
{
    float C = s * v;
    float X = C * (1 - fabs(std::fmod(h / 60.0f, 2) - 1));
    float m = v - C;

    float r1, g1, b1;

    if(h >= 0 && h <= 60)
    {
        r1 = C;
        g1 = X;
        b1 = 0;
    }
    if(h >= 60 && h < 120)
    {
        r1 = X;
        g1 = C;
        b1 = 0;
    }
    if(h >= 120 && h < 180)
    {
        r1 = 0;
        g1 = C;
        b1 = X;
    }
    if(h >= 180 && h < 240)
    {
        r1 = 0;
        g1 = X;
        b1 = C;
    }
    if(h >= 240 && h < 300)
    {
        r1 = X;
        g1 = 0;
        b1 = C;
    }
    if(h >= 300 && h <= 360)
    {
        r1 = C;
        g1 = 0;
        b1 = X;
    }

    return sf::Color((r1 + m) * 255, (g1 + m) * 255, (b1 + m) * 255);
}

void ColorPicker::createRuler()
{
    int r = 255; 
    int g = 0;
    int b = 0;

    for(int horizontal = 0; horizontal < 510; ++horizontal)
    {
        if(horizontal > 0)
        {
            if(r == 255 && g < 255 && b == 0)
            {
                g+=3;
            }
            if(r > 0 && g == 255)
            {
                r-=3;
            }
            if(r == 0 && g == 255 && b < 255)
            {
                b+=3;
            }
            if(g > 0 && b == 255)
            {
                g-=3;
            }
            if(r < 255 && g == 0 && b == 255)
            {
                r+=3;
            }
            if(r == 255 && b > 0)
            {
                b-=3;
            }
        }

        for(int vertical = 0; vertical < 15; ++vertical)
        {
            ruler[(15 * horizontal) + vertical].position = sf::Vector2f(120 + horizontal, 550 + vertical);
            ruler[(15 * horizontal) + vertical].color = sf::Color(r, g, b);
        }
    }
}

void ColorPicker::createPicker(float hue)
{
    double value = 1;
    double saturation = 1;

    for(int row = 0; row < 255; ++row)
    {
        value = 1;
        saturation -= 0.0039f;

        for(int column = 0; column < 255; ++column)
        {
            value -= 0.0039f;

            picker[(255 * row) + column].position = sf::Vector2f(503 - row, 247 + column);
            picker[(255 * row) + column].color = convertHSVtoRGB(hue, saturation, value);
        }
    }
}

void ColorPicker::updatePickerColor(float hue)
{
    double value = 1;
    double saturation = 1;

    for(int row = 0; row < 255; ++row)
    {
        value = 1;
        saturation -= 0.0039f;

        for(int column = 0; column < 255; ++column)
        {
            value -= 0.0039f;

            picker[(255 * row) + column].color = convertHSVtoRGB(hue, saturation, value);
        }
    } 
}

sf::Color ColorPicker::getRulerColor(sf::Vector2f mouseCoords)
{
    return ruler[(mouseCoords.x - 120) * 15].color;
}

sf::Color ColorPicker::getPickerColor(sf::Vector2f mouseCoords)
{
    int index = ((255 - (hueSelector.getPosition().x - 248)) * 255) + (hueSelector.getPosition().y- 247);

    return picker[index].color;
}

void ColorPicker::handlePicker_Input(sf::Vector2f mouseCoords)
{
    if(picker.getBounds().contains(mouseCoords))
    {
        hueSelector.setPosition(mouseCoords);
        hueSelector.setFillColor(getPickerColor(mouseCoords));
    }
}

void ColorPicker::handleRuler_Input(sf::Vector2f mouseCoords)
{
    if(ruler.getBounds().contains(sf::Vector2f(mouseCoords.x, ruler[8].position.y)))
    {
        colorSelector.setPosition(sf::Vector2f(mouseCoords.x, colorSelector.getPosition().y));
        colorSelector.setFillColor(getRulerColor(mouseCoords));

        updatePickerColor(convertRGBtoHSV(getRulerColor(mouseCoords)));  
        hueSelector.setFillColor(getPickerColor(mouseCoords));
    }
    else if(!ruler.getBounds().contains(sf::Vector2f(mouseCoords.x, ruler[8].position.y)) && !ruler.getBounds().contains(colorSelector.getPosition()))
    {
        colorSelector.setPosition(ruler[8].position);
    }
}

void ColorPicker::handleSelected()
{
    settings.setFillColor(hueSelector.getFillColor());

    window.close();
}

void ColorPicker::handleSelection(sf::Vector2f mouseCoords)
{
	while(window.pollEvent(event))
	{
		switch(event.type)
		{
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::Resized:
            {
                sf::FloatRect viewRect(
                    0,
                    0,

                    event.size.width,
                    event.size.height
                );

                sf::View newView(viewRect);

                window.setView(newView);
            }
                break;

			case sf::Event::MouseButtonPressed:
				if(event.mouseButton.button == sf::Mouse::Left)
				{
					if(ruler.getBounds().contains(mouseCoords))
                	{
                    	selected = Ruler;
                        handleRuler_Input(mouseCoords);
                	}
                	else if(picker.getBounds().contains(mouseCoords))
                	{
                   	    selected = Picker;
					    handlePicker_Input(mouseCoords); 
                    }
				}
				break;

			case sf::Event::MouseMoved:
				if(selected == Ruler)
				{
					handleRuler_Input(mouseCoords);
				}
				else if(selected == Picker)
				{
					handlePicker_Input(mouseCoords);
				}
                break;

            case sf::Event::MouseButtonReleased:
                if(event.mouseButton.button == sf::Mouse::Left)
                {
                    selected = None;
                }
                break;
		}

        gui.handleEvent(event);
	}
}

